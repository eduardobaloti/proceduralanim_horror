using System.Collections;
using System.Collections.Generic;
using MoreMountains.Feedbacks;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public GameObject heart;
    public float tempoDecorrido = 0;
    public GameObject[] bulletSprites;
    public int bullets;
    public int life;
    public MMFeedbacks feedbacks;


    void Update()
    {
        tempoDecorrido += Time.deltaTime;
        float t = Mathf.Clamp01(tempoDecorrido / 1);
        Vector3 escalaAtual = Vector3.Lerp(new Vector3(1f, 1f, 1f), new Vector3(1.25f, 1.25f, 1.25f), t);

        heart.transform.localScale = escalaAtual;
        if (tempoDecorrido >= 1) tempoDecorrido = 0.0f;
    }

    public void Shake()
    {
        feedbacks?.PlayFeedbacks();
    }


    public void UpdateBulletCount()
    {
        bulletSprites[bullets - 1].SetActive(false);
        bullets -= 1;
    }
}

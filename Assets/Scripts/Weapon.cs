using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Singleton<Weapon>
{
    bool readyToShot = true;
    public SpriteRenderer weapon;
    public GameObject bullet;



    public void Shot()
    {
        if (!readyToShot) return;
        if (GameManager.Instance.bullets <= 0)
        {
            if (SoundManager.Instance.noBulletSound.isPlaying) return;
            SoundManager.Instance.noBulletSound.Play();
            return;
        }

        GameManager.Instance.UpdateBulletCount();

        readyToShot = false;
        StartCoroutine(ShotDelay());
        SoundManager.Instance.shotSound.Play();
        GameManager.Instance.Shake();

        float aimValue = Player.Instance.aimValue;
        int bulletSide = 1;
        if (!Player.Instance.isFacingRight) bulletSide = -1;

        if (aimValue == 0) Instantiate(bullet, transform.position, Quaternion.Euler(0, 0, 0));
        else if (aimValue > 0) Instantiate(bullet, transform.position, Quaternion.Euler(0, 0, 20 * bulletSide));
        else if (aimValue < 0) Instantiate(bullet, transform.position, Quaternion.Euler(0, 0, -20 * bulletSide));

    }

    IEnumerator ShotDelay()
    {
        yield return new WaitForSecondsRealtime(0.25f);
        readyToShot = true;
    }
}

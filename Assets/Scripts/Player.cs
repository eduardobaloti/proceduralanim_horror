using System.Collections;
using System.Collections.Generic;
using MoreMountains.Feedbacks;
using Unity.VisualScripting;
using UnityEngine;

public class Player : Singleton<Player>
{

    Rigidbody2D rb;
    public GameObject sprites, arm;
    public Animator legs_anim;
    public bool isFacingRight = true;
    int speed = 80;
    public float aimValue;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        float horizontalInput = (Input.GetAxisRaw("Horizontal"));
        float verticalInput = (Input.GetAxisRaw("Vertical"));
        aimValue = verticalInput;

        rb.velocity = new Vector2(horizontalInput * speed * Time.deltaTime, rb.velocity.y);
        Aim(verticalInput);

        if (horizontalInput > 0)
        {
            sprites.transform.localScale = new Vector2(1, 1);
            isFacingRight = true;
        }
        if (horizontalInput < 0)
        {
            sprites.transform.localScale = new Vector2(-1, 1);
            isFacingRight = false;
        }



    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) Weapon.Instance.Shot();

        if (rb.velocity.magnitude > 0.25)
        {
            legs_anim.SetBool("walking", true);
            if (!SoundManager.Instance.steps.isPlaying) SoundManager.Instance.steps.Play();
        }
        else
        {
            legs_anim.SetBool("walking", false);
            if (SoundManager.Instance.steps.isPlaying) SoundManager.Instance.steps.Stop();
        }
    }

    void Aim(float aimValue)
    {
        if (!isFacingRight) aimValue = aimValue * -1;

        if (aimValue == 0) arm.transform.rotation = Quaternion.Euler(0, 0, 0);
        else if (aimValue > 0.1) arm.transform.rotation = Quaternion.Euler(0, 0, 20);
        else if (aimValue < 0.1) arm.transform.rotation = Quaternion.Euler(0, 0, -20);
    }


}

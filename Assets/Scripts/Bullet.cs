using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    void Awake()
    {
        StartCoroutine(DestroyObject());
        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        if (Player.Instance.isFacingRight) rb.AddForce(transform.right * 0.2f);
        else rb.AddForce(transform.right * -0.2f);
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Monster")
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);

        }

    }

    IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }

}
